FROM amancevice/pandas:1.2.1-slim

WORKDIR /app

COPY scripts/requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY scripts/requirements-gadm.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY . .

CMD python scripts/convert_to_csv.py --dest-folder=data && python scripts/convert_to_csv.py --dest-folder=csv
