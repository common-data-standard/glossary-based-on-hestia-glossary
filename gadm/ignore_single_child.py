import pandas as pd


region = pd.read_csv('data/region.csv', usecols=['term.id', 'term.subClassOf.0.id'])
filepath = 'ignore.txt'
open(filepath, 'w+').writelines('')

for index, row in region.iterrows():
    id = row['term.id']
    parent_id = row['term.subClassOf.0.id']
    same_parent_id = region[region['term.subClassOf.0.id'] == parent_id]
    if len(same_parent_id) == 1:
        open(filepath, 'a+').writelines(id + '\n')
