#!/bin/sh

URL=https://geodata.ucdavis.edu/gadm/gadm4.0
DEST=downloads
rm -rf $DEST
mkdir -p $DEST

# gadm36_shp.zip
# gadm36_levels_shp.zip
# shp/gadm36_AFG_shp.zip
FILE=${1:-"gadm36_levels_shp.zip"}

curl -L -o "$DEST/$FILE" -X GET "$URL/$FILE"

mkdir -p shapefiles
unzip "$DEST/$FILE" -d shapefiles/
