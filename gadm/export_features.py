# run pip install shapely
import os
import argparse
import traceback
from functools import reduce
from concurrent.futures import ThreadPoolExecutor
import time
import geopandas as gp


parser = argparse.ArgumentParser()
parser.add_argument('--version', type=int, default=36,
                    help='Current GADM version')
parser.add_argument('--code', type=str,
                    help='The 3-letter GADM country code')
parser.add_argument('--level', type=int,
                    help='0 to 5 - run for specific level only')
parser.add_argument('--limit', type=int,
                    help='Limit the number of files to handle. Only works if --code is not set.')
parser.add_argument('--offset', type=int, default=0,
                    help='Combine with --limit to paginate through the files. Only works if --code is not set.')
parser.add_argument('--output-dir', type=str, default='data',
                    help='Export to directory. Defaults to "data".')
args = parser.parse_args()


INPUT_DIR = 'files'
# gadm Ids which name is "None"
NONE_IDS = [
    'GADM-ITA.13.6.168_1'
]


def current_time(): return int(round(time.time() * 1000))


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def non_empty_list(values: list): return list(filter(lambda x: len(x) > 0, values))


def paginate(values: list, offset=0, limit=None): return values[offset:(limit + offset if limit is not None else None)]


with open('./duplicates.txt', 'r') as f:
    duplicates = non_empty_list(f.read().split('\n'))


def row_to_geojson(level: int, row: dict):
    gid = row['GID_' + level]
    gadm_id = 'GADM-' + gid
    gname = row['NAME_' + level]
    data = gp.GeoSeries(row['geometry'])
    return (gadm_id, data.to_json()) if gname is not None and (gname != 'None' or gadm_id in NONE_IDS) else (None, {})


def handle_file(file: dict):
    try:
        zones = gp.read_file(file.get('input'), layer=file.get('layer'), encoding='utf-8')
        print('generating', file.get('input'))

        for index, row in zones.iterrows():
            try:
                gadm_id, data = row_to_geojson(str(file.get('level')), row)
                if gadm_id is not None:
                    ouput = f"{args.output_dir}/{gadm_id}.geojson"
                    with open(ouput, 'w') as f:
                        f.write(data)
            except Exception:
                print(traceback.format_exc())
        return ''
    except ValueError:
        return ''


def file_args(code: str, level):
    filepath = f"zip://{INPUT_DIR}/gadm{args.version}_{code}_shp.zip"
    layer = f"gadm{args.version}_{code}_{level}"
    return {
        'input': filepath,
        'layer': layer,
        'code': code,
        'level': level
    }


def compute_files():
    levels = [args.level] if args.level is not None else [0, 1, 2, 3, 4, 5]
    files = list(map(lambda x: f"gadm{args.version}_{x}_shp.zip", args.code.split(','))) if args.code \
        else paginate(sorted(os.listdir(f"{INPUT_DIR}")), args.offset, args.limit)
    print('importing files', files)
    codes = list(map(lambda f: f.replace(f"gadm{args.version}_", '').replace('_shp.zip', ''), files))
    files = []
    for code in codes:
        for level in levels:
            fargs = file_args(code, level)
            files.extend([fargs])
    return files


def main():
    now = current_time()
    files = compute_files()

    with ThreadPoolExecutor() as executor:
        list(executor.map(handle_file, files))

    print(f"took {current_time() - now} ms")


if __name__ == "__main__":
    main()
