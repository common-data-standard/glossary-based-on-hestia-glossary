import pandas as pd
import geopandas as gp


source = pd.read_csv('iso3166-2.csv', index_col=None)
dest = pd.read_csv('mappings.csv', index_col=None)


names_by_code = {}
for index, row in source.iterrows():
    region = dest.loc[dest['iso31662Code'] == row['code']].to_dict('records')
    if len(region) > 0:
        country_code = region[0]['gadmId'][5:]
        if country_code not in names_by_code:
            names_by_code[country_code] = []

        names_by_code[country_code].append({'name': row['name'], 'code': row['iso31662Code']})


def find_by_level(level: str):
    for code, values in names_by_code.items():
        filepath = f"zip://files/gadm36_{code}_shp.zip"
        try:
            zones = gp.read_file(filepath, layer=f"gadm36_{code}_{level}", encoding='utf-8')

            for value in values:
                name = value['name']
                zone = zones.loc[zones['NAME_' + level] == name].to_dict('records')
                # try in synonyms as well
                if len(zone) == 0:
                    zone = zones.loc[zones['VARNAME_' + level] == name].to_dict('records')

                if len(zone) > 0:
                    row = ['GADM-' + zone[0]['GID_' + level], zone[0]['NAME_' + level], value['code'], '', '']
                    open('data/mappings.csv', 'a+').writelines(','.join(row) + '\n')
                else:
                    row = [code, name, value['code']]
                    open('data/missing.csv', 'a+').writelines(','.join(row) + '\n')
        except ValueError:
            continue


list(map(find_by_level, ['1']))
