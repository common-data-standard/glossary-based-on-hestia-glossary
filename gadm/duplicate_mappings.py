import os
from functools import reduce
import pandas as pd

DATA_DIR = 'csv'


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def clean_values(data):
    def get_id(value):
        values = str(value).split('/')
        return values[1] if len(values) == 2 else values[0]

    values = []
    for index, row in data.iterrows():
        id = get_id(row['id'])
        values.append({'id': id, 'name': row['name']})

    return values


def find_duplicates():
    def get_names(folder: str):
        def get_all(filename: str):
            try:
                filepath = f"{folder}/{filename}"
                return filepath.replace('.csv', '.xlsx'), clean_values(pd.read_csv(f"{DATA_DIR}/{filepath}"))
            except Exception as e:
                raise Exception(str(e), folder, filename)

        return get_all

    def get_folder_names(folder: str):
        files = list(filter(lambda file: file.endswith('.csv'), os.listdir(f"{DATA_DIR}/{folder}")))
        return list(map(get_names(folder), files))

    folders = list(filter(os.path.isdir, os.listdir(DATA_DIR)))
    names_by_file = list(reduce(lambda prev, curr: prev + get_folder_names(curr), folders, []))

    mapping = {}
    for filename, values in names_by_file:
        for val in values:
            name = val.get('name')
            mapping[name] = [] if name not in mapping else mapping[name]
            mapping[name].append(val.get('id'))
    return mapping


rows = flatten(list(filter(lambda x: len(x) > 1, find_duplicates().values())))
duplicates = list(filter(lambda x: x.startswith('GADM-') and len(x) > 8, rows))
open('gadm/duplicates.txt', 'w+').writelines('\n'.join(duplicates))
