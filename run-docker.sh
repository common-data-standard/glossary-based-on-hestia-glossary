#!/bin/sh
rm -rf data/*.csv
rm -rf csv
docker build -t hestia-glossary:test .
docker run --rm \
  --name hestia-glossary \
  -v ${PWD}/data:/app/data \
  -v ${PWD}/csv:/app/csv \
  hestia-glossary:test
