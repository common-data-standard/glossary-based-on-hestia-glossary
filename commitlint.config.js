const { readdirSync, statSync } = require('fs');
const { resolve, join } = require('path');

const root = resolve(__dirname);
const folders = readdirSync(root)
  .filter(folder => statSync(folder).isDirectory())
  .filter(folder => /[A-Z]/.test(folder[0]));

const typeScopes = folders
  .flatMap(folder => readdirSync(join(root, folder)))
  .filter(file => file.endsWith('.xlsx') && !file.includes('-'))
  .map(file => file.replace('.xlsx', ''));

const extraScopes = [
  'gadm',
  'ecoClimateZone',
  'ecoregion',
  'scripts',
  'package',
  'readme',
  'contributing',
  'license',
  'gitlab'
];

module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    'scope-case': [0],
    'scope-enum': [2, 'always', [...typeScopes, ...extraScopes]],
    'footer-max-line-length': [2, 'always', 120]
  }
};
