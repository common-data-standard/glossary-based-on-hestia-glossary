**Source:** [IPCC (2019) 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories](https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch05_Cropland.pdf).

<!--

# Column Mapping

| term.id | region |
| ------- | ------ |
| GADM-AZE | Asia |
| GADM-BHS | South America |
| GADM-BHR | Asia |
| GADM-BGD | South Asia |
| GADM-BRB | South America |
| GADM-BLR | Europe |
| GADM-BEL | Europe |
| GADM-BLZ | South America |
| GADM-BEN | Africa |
| GADM-BMU | South America |
| GADM-BTN | South Asia |
| GADM-BOL | South America |
| GADM-BIH | Europe |
| GADM-BWA | Africa |
| GADM-BRA | South America |
| GADM-VGB | South America |
| GADM-BRN | South East Asia |
| GADM-BGR | Europe |
| GADM-BFA | Africa |
| GADM-BDI | Africa |
| GADM-KHM | South East Asia |
| GADM-CMR | Africa |
| GADM-CAN | North America |
| GADM-CPV | Africa |
| GADM-CYM | South America |
| GADM-CAF | Africa |
| GADM-TCD | Africa |
| GADM-CHL | South America |
| GADM-CHN | East Asia |
| GADM-COL | South America |
| GADM-COM | Africa |
| GADM-COG | Africa |
| GADM-COK | Oceania |
| GADM-CRI | South America |
| GADM-CIV | Africa |
| GADM-HRV | Europe |
| GADM-CUB | South America |
| GADM-CYP | Europe |
| GADM-CZE | Europe |
| GADM-PRK | East Asia |
| GADM-COD | Africa |
| GADM-DNK | Europe |
| GADM-DJI | Africa |
| GADM-DMA | South America |
| GADM-DOM | South America |
| GADM-ECU | South America |
| GADM-EGY | Africa |
| GADM-SLV | South America |
| GADM-GNQ | Africa |
| GADM-ERI | Africa |
| GADM-EST | Europe |
| GADM-ETH | Africa |
| GADM-FRO | Europe |
| GADM-FJI | Oceania |
| GADM-FIN | Europe |
| GADM-FRA | Europe |
| GADM-GUF | South America |
| GADM-PYF | Oceania |
| GADM-GAB | Africa |
| GADM-GMB | Africa |
| GADM-GEO | Asia |
| GADM-DEU | Europe |
| GADM-GHA | Africa |
| GADM-GRC | Europe |
| GADM-GRD | South America |
| GADM-GLP | South America |
| GADM-GUM | Oceania |
| GADM-GTM | South America |
| GADM-GIN | Africa |
| GADM-GNB | Africa |
| GADM-GUY | South America |
| GADM-HTI | South America |
| GADM-HND | South America |
| GADM-HUN | Europe |
| GADM-ISL | Europe |
| GADM-IND | South Asia |
| GADM-IDN | South East Asia |
| GADM-IRN | Asia |
| GADM-IRQ | Asia |
| GADM-IRL | Europe |
| GADM-ISR | Asia |
| GADM-ITA | Europe |
| GADM-JAM | South America |
| GADM-JPN | East Asia |
| GADM-JOR | Asia |
| GADM-KAZ | Asia |
| GADM-KEN | Africa |
| GADM-KIR | Oceania |
| GADM-KWT | Asia |
| GADM-KGZ | Asia |
| GADM-LAO | South East Asia |
| GADM-LVA | Europe |
| GADM-LBN | Asia |
| GADM-LSO | Asia |
| GADM-LBR | Asia |
| GADM-LBY | Asia |
| GADM-LTU | Europe |
| GADM-LUX | Europe |
| GADM-MDG | Africa |
| GADM-MWI | Africa |
| GADM-MYS | South East Asia |
| GADM-MDV | South Asia |
| GADM-MLI | Africa |
| GADM-MLT | Europe |
| GADM-MHL | Oceania |
| GADM-MTQ | South America |
| GADM-MRT | Africa |
| GADM-MUS | Africa |
| GADM-MEX | South America |
| GADM-FSM | Oceania |
| GADM-MNG | Asia |
| GADM-MNE | Europe |
| GADM-MSR | South America |
| GADM-MAR | Africa |
| GADM-MOZ | Africa |
| GADM-MMR | South East Asia |
| GADM-NAM | Africa |
| GADM-NRU | Oceania |
| GADM-NPL | South Asia |
| GADM-NLD | Europe |
| GADM-NCL | Oceania |
| GADM-NZL | Oceania |
| GADM-NIC | South America |
| GADM-NER | Africa |
| GADM-NGA | Africa |
| GADM-NIU | Oceania |
| GADM-NOR | Europe |
| GADM-PSE | Asia |
| GADM-OMN | Asia |
| GADM-PAK | South Asia |
| GADM-PAN | South America |
| GADM-PNG | Oceania |
| GADM-PRY | South America |
| GADM-PER | South America |
| GADM-PHL | South America |
| GADM-POL | Europe |
| GADM-PRT | Europe |
| GADM-PRI | South America |
| GADM-QAT | Asia |
| GADM-KOR | East Asia |
| GADM-MDA | Europe |
| GADM-REU | Africa |
| GADM-ROU | Europe |
| GADM-RUS | Asia |
| GADM-RWA | Africa |
| GADM-KNA | South America |
| GADM-LCA | South America |
| GADM-SPM | North America |
| GADM-VCT | South America |
| GADM-WSM | Oceania |
| GADM-STP | Africa |
| GADM-SAU | Asia |
| GADM-SEN | Africa |
| GADM-SRB | Europe |
| GADM-SYC | Africa |
| GADM-SLE | Africa |
| GADM-SGP | South East Asia |
| GADM-SVK | Europe |
| GADM-SVN | Europe |
| GADM-SLB | Oceania |
| GADM-SOM | Africa |
| GADM-ZAF | Africa |
| GADM-ESP | Europe |
| GADM-LKA | South Asia |
| GADM-SDN | Africa |
| GADM-SUR | South America |
| GADM-SWZ | Africa |
| GADM-SWE | Europe |
| GADM-CHE | Africa |
| GADM-SYR | Asia |
| GADM-TJK | Asia |
| GADM-THA | South East Asia |
| GADM-MKD | Europe |
| GADM-TLS | South East Asia |
| GADM-TGO | Africa |
| GADM-TKL | Oceania |
| GADM-TON | Oceania |
| GADM-TTO | South America |
| GADM-TUN | Africa |
| GADM-TUR | Asia |
| GADM-TKM | Asia |
| GADM-TUV | Oceania |
| GADM-UGA | Africa |
| GADM-UKR | Europe |
| GADM-ARE | Asia |
| GADM-GBR | Europe |
| GADM-TZA | Africa |
| GADM-USA | North America |
| GADM-URY | South America |
| GADM-UZB | Asia |
| GADM-VUT | Oceania |
| GADM-VEN | South America |
| GADM-VNM | South East Asia |
| GADM-WLF | Oceania |
| GADM-ESH | Africa |
| GADM-YEM | Asia |
| GADM-ZMB | Africa |
| GADM-ZWE | Africa |
| GADM-XPI | South East Asia |
| GADM-CUW | South America |
| GADM-AND | Europe |
| GADM-VIR | South America |
| GADM-UMI | North America |
| GADM-MAC | East Asia |
| GADM-SXM | South America |
| GADM-IMN | Europe |
| GADM-MAF | South America |
| GADM-ALA | Europe |
| GADM-XAD | Europe |
| GADM-VAT | Europe |
| GADM-TWN | East Asia |
| GADM-LIE | Europe |
| GADM-GIB | Europe |
| GADM-GGY | Europe |
| GADM-SSD | Africa |
| GADM-SJM | Europe |
| GADM-BES | South America |
| GADM-JEY | Europe |
| GADM-XKO | Europe |
| GADM-XNC | Europe |
| GADM-SMR | Europe |
| GADM-PCN | Oceania |
| GADM-HMD | Oceania |
| GADM-XSP | South East Asia |
| GADM-HKG | East Asia |
| GADM-AIA | South America |
| GADM-ABW | South America |
| GADM-MYT | Africa |
| GADM-BLM | South America |
| GADM-TCA | South America |
| GADM-MCO | Europe |

-->
