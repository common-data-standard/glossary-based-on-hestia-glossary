**Source:** [Blonk Consultants Direct Land Use Change Assessment Tool version 2014-01-21](https://www.blonkconsultants.nl/portfolio-item/direct-land-use-change-tool/), adjusted by [Poore & Nemecek (2018)](http://science.sciencemag.org/content/360/6392/987/).

<!--

# Changelog

## 2021-05-17

- ensure number of columns matches original source
- add pasture and cropland values
- rename file to say "land use change" and use camel case

## 2021-05-06

- rename column `Cottonseed` to `Seed cotton`

## 2021-05-05

- add missing columns for `cropGroupingFAOSTAT`: `Cotton lint`, `Cottonseed`, `Gums, natural`, `Grain, mixed`, `Palm kernels`, `Oil, palm`, `Rubber, natural`

-->
