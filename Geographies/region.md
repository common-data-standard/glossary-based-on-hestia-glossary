**Source:** [GADM 3.6](https://gadm.org/data.html).

<!--

# Changelog

## 2021-07-20

- update `term.name` on duplicate region names when ignoring case and accents

-->
