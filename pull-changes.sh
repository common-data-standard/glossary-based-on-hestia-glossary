#!/usr/bin/env bash

echo "Pulling changes..."

git clone https://${GITLAB_CI_USER}:${GITLAB_CI_TOKEN}@gitlab.com/hestia-earth/${DEPLOY_REPO}.git &>/dev/null
cd $DEPLOY_REPO
git checkout $TARGET_BRANCH
# either fetch existing branch or create new one
git checkout $SOURCE_BRANCH || (git checkout -b $SOURCE_BRANCH && git branch -u origin/$SOURCE_BRANCH)
# keep only 1 commit from target (automatic squash)
git reset --hard $TARGET_BRANCH

rm -rf lookups && cp -rf ../lookups lookups
rm -rf data && cp -rf ../data data
rm -rf csv && cp -rf ../csv csv
