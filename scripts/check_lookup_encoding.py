import sys
import glob
from chardet.universaldetector import UniversalDetector

detector = UniversalDetector()
ALLOWED_ENCODING = [
    'ascii',
    'ISO-8859-1',
    'utf-8'
]

def get_encoding():
    results = []

    for filename in glob.glob('**/*-lookup.csv'):
        detector.reset()

        for line in open(filename, 'rb'):
            detector.feed(line)
            if detector.done: break
        detector.close()

        results.append({'filename': filename, 'encoding': detector.result['encoding']})

    return results


def main(args: list):
    exit_code = ''

    for result in get_encoding():
        filename = result.get('filename')
        encoding = result.get('encoding')

        if encoding not in ALLOWED_ENCODING:
            exit_code += f"\nWrong encoding for {filename}: {encoding}"

    sys.exit(0 if exit_code == '' else f"Allowed encodings: {ALLOWED_ENCODING}{exit_code}")


if __name__ == "__main__":
    main(sys.argv[1:])
