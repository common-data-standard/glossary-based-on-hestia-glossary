# install: `pip -r install scripts/requirements-gadm.txt`
# add file `ee-credentials.json` to authenticate to Google Earth Engine
# usage: `python scripts/GADM_generate_lookup.py <level> <collection> <collection field> <lookup src file> <override existing>`
# examples:
#   - `python scripts/GADM_generate_lookup.py 1 "users/hestiaplatform/AWARE" Name lookups/awareWaterBasinId.csv`
#   - `python scripts/GADM_generate_lookup.py 1 "users/hestiaplatform/Terrestrial_Ecoregions_World" eco_code Geographies/ecoregion-factors-lookup.csv`
from statistics import mean
import sys
import os
import traceback
import pandas as pd
import numpy as np
from concurrent.futures import ThreadPoolExecutor
import ee

MAX_WORKERS = 10
AREA_FIELD = 'areaKm2'
AREA_PERCENT_FIELD = 'areaKm2_percent'


def area_km2(geometry: ee.Geometry): return geometry.area().divide(1000 * 1000)


def add_area(region: ee.Feature): return region.set({AREA_FIELD: area_km2(region.geometry())})


def add_area_percent(total: float):
    def add(region: ee.Feature):
        return region.set({AREA_PERCENT_FIELD: ee.Number(region.get(AREA_FIELD)).multiply(100).divide(total)})
    return add


def aggregate_area(collection: ee.FeatureCollection):
    collection = collection.map(add_area)
    total_area = collection.aggregate_sum(AREA_FIELD).getInfo()
    return collection.map(add_area_percent(total_area)).getInfo().get('features', [])


def intersect(geometry): return lambda feature: feature.intersection(geometry, 1)


def clip_collection(collection: ee.FeatureCollection, geometry: ee.Geometry): return collection.map(intersect(geometry))


def _id_to_level(id: str): return id.count('.')


def load_region(gadm_id: str):
    level = _id_to_level(gadm_id)
    id = gadm_id.replace('GADM-', '')
    return ee.FeatureCollection(f"users/hestiaplatform/gadm36_{level}").filterMetadata(f"GID_{level}", 'equals', id)


def non_empty_list(values: list): return list(filter(lambda x: len(x) > 0, values))


def _all_gadm_ids(level: str):
    with open(f"./scripts/GADM-ID-level{level}.txt", 'r') as f:
        return non_empty_list(f.read().split('\n'))


def _load_sub_regions(id: str):
    try:
        import json
        with open(f'./scripts/{id}.json') as f:
            return list(map(lambda x: x.get('@id'), json.load(f).get('results')))
    except Exception:
        return None


def collection_data(collection_name: str, field: str, lookup: pd.DataFrame, columns: list, id: str):
    region = load_region(id)
    collection = ee.FeatureCollection(collection_name).filterBounds(region)
    values = []
    try:
        features = aggregate_area(clip_collection(collection, region.geometry()))
        for col in columns:
            weighted_values = []
            for feature in features:
                try:
                    props = feature.get('properties', {})
                    row_id = props.get(field)
                    percent = props.get(AREA_PERCENT_FIELD)
                    value = lookup.loc[int(row_id) if row_id.isnumeric() else row_id][col]
                    if value != '-' and value != '' and not np.isnan(value):
                        weighted_values.append([value, percent])
                except Exception as e:
                    print('Skip value', col, id, str(e))

            total_value = sum([value * percent for value, percent in weighted_values])
            total_percent = sum(percent for _v, percent in weighted_values)
            value = total_value / total_percent if total_percent > 0 else '-'
            values.append(str(value))

    except Exception:
        stack = traceback.format_exc()
        print(id, stack)
        values = ['-'] * len(columns)
    return values


def load_from_subregions(sub_regions: list, columns: list, lookup: pd.DataFrame):
    values = []
    for col in columns:
        col_values = []
        for subregion in sub_regions:
            try:
                value = lookup.loc[subregion][col]
                if value != '-':
                    col_values.append(float(value))
            except KeyError:
                continue
        if len(col_values) == 0:
            raise Exception('subregions not processed yet, processing')
        values.append(str(mean(col_values)))
    return values


def gadm_data(collection_name: str, field: str, filepath: str, lookup: pd.DataFrame, columns: list):
    def run(id: str):
        print('processing', id)
        sub_regions = _load_sub_regions(id)
        if sub_regions:
            try:
                values = load_from_subregions(sub_regions, columns, lookup)
            except Exception:
                print('processing sub-regions')
                with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
                    list(executor.map(run, sub_regions))
                values = load_from_subregions(sub_regions, columns, lookup)
        else:
            values = collection_data(collection_name, field, lookup, columns, id)

        row = [id] + values
        open(filepath, 'a+', encoding='utf-8').writelines(','.join(row) + '\n')
        return True
    return run


def generate_data(filepath: str, collection: str, field: str, gadm_ids: list, lookup: pd.DataFrame, override_existing):
    columns = list(lookup.columns.values)
    existing_ids = []

    # create file it not exists, otherwise append data only
    if not os.path.exists(filepath):
        open(filepath, 'w', encoding='utf-8').close()
        open(filepath, 'a+', encoding='utf-8').writelines(','.join(['term.id'] + columns) + '\n')
    else:
        with open(filepath, 'r') as f:
            data = f.readlines()[1:]
        existing_ids = [v.split(',')[0] for v in data]

    # remove already processed ids
    ids = [id for id in gadm_ids if override_existing or id not in existing_ids]

    with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        return list(executor.map(gadm_data(collection, field, filepath, lookup, columns), ids))


def main(args):
    ee.Initialize(ee.ServiceAccountCredentials(None, 'ee-credentials.json'))

    level = args[0]
    collection = args[1]
    field = args[2]
    lookup_file = args[3]
    override_existing = bool(args[4]) if len(args) > 4 else False

    paths = lookup_file.split('/')
    directory = '/'.join(paths[:-1])
    output_file = f"{directory}/region-{paths[-1].split('.')[0]}.csv"

    gadm_ids = _all_gadm_ids(str(level))
    lookup = pd.read_csv(lookup_file, index_col=0)

    generate_data(output_file, collection, field, gadm_ids, lookup, override_existing)


if __name__ == "__main__":
    main(sys.argv[1:])
