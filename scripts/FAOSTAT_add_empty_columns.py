import sys
import pandas as pd


def non_mepty_list(values: list): return list(filter(lambda x: len(x) > 0, values))


def main(args):
    file = args[0]
    df = pd.read_csv(file, index_col=None)

    headers_file = args[1]
    with open(headers_file, 'r') as f:
        headers = non_mepty_list(f.read().split('\n'))

    for header in headers:
        df[header] = '-'

    df.to_csv(file, index=False)


if __name__ == "__main__":
    main(sys.argv[1:])
