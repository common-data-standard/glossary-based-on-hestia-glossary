# checks every -lookup.csv file for matching term.id and move to lookups folder if valid
import sys
import os
from shutil import copyfile
import pandas as pd
from functools import reduce
from concurrent.futures import ThreadPoolExecutor


EXTENSION = '.csv'
SUFFIX = '-lookup.csv'
BASE_DIR = './'
DEST_DIR = 'lookups'
EXCLUDED_FOLDERS = ['csv', 'data', 'envs', 'scripts']


def mkdirs(dest: str):
    os.makedirs(dest, exist_ok=True)


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def handle_lookup(folder: str, filename: str, merging: bool):
    src_filepath = os.path.join(folder, filename)
    dest_filepath = os.path.join(DEST_DIR, filename.replace(SUFFIX, EXTENSION))

    if merging:
        src_df = pd.read_csv(src_filepath, index_col=0)
        # merge existing lookup with current one
        dest_df = pd.read_csv(dest_filepath, index_col=0) if os.path.isfile(dest_filepath) else None
        df = pd.concat([src_df, dest_df], axis=1) if dest_df is not None else src_df
        df.to_csv(dest_filepath)
    else:
        copyfile(src_filepath, dest_filepath)


def handle_file(data: dict): return handle_lookup(data.get('folder'), data.get('filename'), data.get('merge', False))


def convert_folder(folder: str):
    folderpath = os.path.join(BASE_DIR, folder)
    if os.path.isdir(folderpath):
        files = list(filter(lambda f: f.endswith(SUFFIX), os.listdir(folderpath)))
        return list(map(lambda f: {'folder': folderpath, 'filename': f, 'merge': len(f.split('-')) == 2}, files))
    return []


def main(args):
    folders = args[0].split(',') if len(args) > 0 \
        else list(filter(lambda f: f not in EXCLUDED_FOLDERS, os.listdir(BASE_DIR)))
    files = flatten(list(map(convert_folder, folders)))
    mkdirs(DEST_DIR)
    with ThreadPoolExecutor() as executor:
        return list(executor.map(handle_file, files))


if __name__ == "__main__":
    main(sys.argv[1:])
