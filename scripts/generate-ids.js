// Generate txt files containing the list of GADM regions ID per level
// node scripts/generate-ids.js <level 0 to 5>
const { promises } = require('fs');
const { join } = require('path');
const { request } = require('https');

const [level] = process.argv.slice(2);
const URL = 'https://api.hestia.earth/search';
const encoding = 'utf8';

const { writeFile } = promises;

const search = (body) => new Promise((resolve, reject) => {
  const req = request(URL, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' }
  }, res => {
    res.setEncoding(encoding);
    const data = [];
    res.on('data', chunk => data.push(chunk));
    res.on('end', () => resolve(JSON.parse(data.join(''))));
    res.on('error', reject);
  });
  req.write(JSON.stringify(body));
  req.end();
});

const run = async () => {
  const { results } = await search({
    limit: 10000,
    fields: ['@id'],
    sort: [{'@id.keyword': 'asc'}],
    query: {
      bool: {
        must: [
          { match: { '@type': 'Term' } },
          { match: { 'termType.keyword': 'region' } },
          { match: { gadmLevel: level } }
        ]
      }
    }
  });
  const ids = results.map(res => res['@id']);
  await writeFile(join(__dirname, `GADM-ID-level${level}.txt`), ids.join('\n'));
};

run()
  .then(() => process.exit(0))
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
