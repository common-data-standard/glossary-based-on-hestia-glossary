# extract properties from feedipedia
import sys
import argparse
from datetime import date
import traceback
import requests
import pandas as pd
from bs4 import BeautifulSoup


PROPERTY_NAME = 'feedipediaName'
CONVERSION_COLUMN = 'feedipediaConversionEnum'
DM_PROP = 'Dry matter'
SKIP_COLS = ['', 'Unit', 'Nb']


parser = argparse.ArgumentParser(description='Extract feedipedia data from properties')
parser.add_argument('--input-file', type=str, required=True,
                    help='The path to the file containing the Terms. Example: "Inputs & Products/crop.xlsx"')
parser.add_argument('--skip-md', action='store_true',
                    help='Skip generating the markdown file.')
parser.add_argument('--term-id', type=str,
                    help='Convert single Term in the file, for testing.')
args = parser.parse_args()


def non_empty_value(value): return str(value) != '-' and str(value) != '' and pd.notna(value)


def float_precision(value): return "{:.5f}".format(float(value) if type(value) == str else value)


def default_dm_value(term):
    value = term['term.defaultProperties.0.value']
    return float(value) if value and value != '-' else 0


def row_to_list(row):
    def td_content(td):
        try:
            return td.a.string.strip()
        except Exception:
            return td.string.strip() if td.string is not None else ''

    return [td_content(td) for td in row.find_all('td') if td is not None]


def clean_value(value):
    return float(value.replace('*', ''))


_CONVERSION_FORMULA = {
    'dmToFmSimple': lambda value, dm_value: float_precision(value * (dm_value / 100)),
    'dmToFm10FactorCorrection': lambda value, dm_value: float_precision(value * (dm_value / 100) / 10),
    'dmToFm10000FactorCorrection': lambda value, dm_value: float_precision(value * (dm_value / 100) / 10000)
}


def _convert_property_value(conversion_formula: str, col_name: str, value: float, dm_value: float):
    if conversion_formula and conversion_formula not in _CONVERSION_FORMULA:
        raise Exception(f"Missing {CONVERSION_COLUMN}: '{conversion_formula}'")

    should_convert = all([
        col_name in ['Avg', 'Min', 'Max', 'SD'],
        value != '',
        conversion_formula
    ])
    # if we should convert, but `dryMatter` is missing => no value
    return '' if should_convert and not dm_value else (
        _CONVERSION_FORMULA[conversion_formula](value, dm_value) if should_convert else value
    )


def _extract_values(headers: list, cols: list, property: dict, dm_value: float):
    prop_values = []
    for index, col in enumerate(cols[1:]):
        if col != '' and len(headers) > index + 1 and headers[index + 1] not in SKIP_COLS:
            col_name = headers[index + 1]
            value = _convert_property_value(property.get(CONVERSION_COLUMN), col_name, clean_value(col), dm_value)
            if value != '':
                prop_values.append(':'.join([col_name, str(value)]))
    return ';'.join(prop_values)


def _find_property(tables, property_name: str):
    for table in tables:
        rows = table.find_all('tr')
        first_row = rows[0]
        if 'tableheader' in first_row.attrs.get('class', []):
            headers = row_to_list(first_row)
            for row in rows[1:]:
                cols = row_to_list(row)
                prop_name = cols[0]
                if prop_name == property_name:
                    for index, col in enumerate(cols[1:]):
                        if col != '' and len(headers) > index + 1 and headers[index + 1] == 'Avg':
                            return clean_value(col)
    return 0


def _extract_term(term: dict, url: str, properties: list, default_dm_value: float):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')

    # make sure the url points to a correct page
    has_field_content = len(soup.find_all('div', {'class': 'field-content'}))
    if has_field_content:
        return None

    tables = soup.find_all('table')

    term_id = term['term.id']
    row_dict = {'term.id': term_id}
    dm_value = _find_property(tables, DM_PROP) or default_dm_value
    if not dm_value:
        print('Warning: missing dry matter property and table value for', term['term.id'])

    for table in tables:
        rows = table.find_all('tr')
        first_row = rows[0]
        if 'tableheader' in first_row.attrs.get('class', []):
            headers = row_to_list(first_row)
            for row in rows[1:]:
                cols = row_to_list(row)
                prop_name = cols[0]
                matching_properties = list(filter(lambda v: v[PROPERTY_NAME] == prop_name, properties))

                # multiple properties can have the same name, add them all
                for property in matching_properties:
                    value = _extract_values(headers, cols, property, dm_value)
                    if value != '':
                        row_dict[property['term.id']] = value

    return row_dict, dm_value


def _extract(src_file: str):
    src_df = pd.read_excel(src_file)
    properties_df = pd.read_csv('lookups/property.csv', index_col=None)
    properties = properties_df[['term.id', PROPERTY_NAME, CONVERSION_COLUMN]].fillna('').to_dict('records')
    rows = []
    errors = []
    for index, term in src_df.iterrows():
        if args.term_id and term['term.id'] != args.term_id:
            continue

        link = term['term.feedipedia']
        if non_empty_value(link):
            dm_value = default_dm_value(term)
            try:
                row_dict, dm_value = _extract_term(term, link, properties, dm_value)
                if row_dict:
                    rows.append(row_dict)
                else:
                    print(f"{term['term.id']} / {link} does not contain the correct content. Please verify it points to a single factsheet.")
            except ConnectionResetError:
                stack = traceback.format_exc()
                print(f"A network error occured while fetching {link}:\n\t{stack}")
            except Exception:
                stack = traceback.format_exc()
                errors.append(f"Error for {term['term.id']} (link={link}):\n\t{stack}")

    dest_file = src_file.replace('.xlsx', '-property-lookup.csv')
    pd.DataFrame.from_records(rows).to_csv(dest_file, index=False, na_rep='')

    if not args.skip_md:
        filepath = src_file.replace('.xlsx', '-property-lookup.md')
        with open(filepath, 'w') as f:
            f.write(f"**Source:** [feedipedia](https://www.feedipedia.org), Auto-generated on {date.today()}.")

    exit_code = '\n'.join(errors) if len(errors) > 0 else 0
    sys.exit(exit_code)


def main():
    _extract(args.input_file)


if __name__ == "__main__":
    main()
