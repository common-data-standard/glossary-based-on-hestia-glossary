#!/usr/bin/env bash

set -e

COMMIT_SHA="$1"
DIFFS=$(git diff --name-only $COMMIT_SHA origin/develop) || ""

function check_changes() {
  termType="$1"
  src="Inputs & Products/${termType}.xlsx"
  lookup="Inputs & Products/${termType}-property-lookup.csv"

  if [[ "$DIFFS" == *"$src"* ]] || [[ "$DIFFS" == *"$lookup"* ]] || [[ "$DIFFS" == *"Properties/property.xlsx"* ]]; then
    echo "Exctracting properties for ${src}..." 1>&2

    # compute property file
    # result will contain information if there are warnings
    res=$(python scripts/extract_f_properties_lookup.py --input-file "$src" --skip-md)

    # check diffs again
    diffs=$(git diff --name-only) || ""
    if [[ "$diffs" == *"$lookup"* ]]; then
      echo "Changes have been detected in ${lookup}." 1>&2
      echo "Please check https://gitlab.com/hestia-earth/hestia-glossary/-/blob/develop/scripts/extract_f_properties_lookup.md for instructions and commit changes." 1>&2
      echo "This script automatically gets data from Feedipedia for key ${termType} data (like nutrient contents) so is critical to run if new ${termType} are added." 1>&2
      echo 1
    elif [[ $res ]]; then
      echo "Extraction of properties finished with warnings:" 1>&2
      echo "$res" 1>&2
      echo 2
    else
      echo 0
    fi
  fi
}

exit_code_crop=$(check_changes "crop")
exit_code_forage=$(check_changes "forage")
exit_code_processedFood=$(check_changes "processedFood")

if [[ $exit_code_crop == 1 ]] || [[ $exit_code_forage == 1 ]] || [[ $exit_code_processedFood == 1 ]]; then
  # TODO: always return a warning because feedipedia website might be unresponsive. Need to look at logs to make sure it is not another issue.
  # exit 1
  exit 2
elif [[ $exit_code_crop == 2 ]] || [[ $exit_code_forage == 2 ]] || [[ $exit_code_processedFood == 2 ]]; then
  exit 2
fi

exit 0
