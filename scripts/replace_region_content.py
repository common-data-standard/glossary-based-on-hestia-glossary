# script to replace data in the region.xlsx file, to avoid making unwanted changes
# install python packages: pandas openpyxl
# example: python scripts/replace_region_content.py --term-id="GADM-USA" --column="term.name" --value="Yes We Can!"
from os.path import dirname, join, abspath
import argparse
import pandas as pd


CURRENT_DIR = dirname(abspath(__file__))
SRC_DIR = join(CURRENT_DIR, '..', 'Geographies')
filename = 'region.xlsx'

parser = argparse.ArgumentParser(description='Generating FAOSTAT data')
parser.add_argument('--term-id', type=str, required=True,
                    help='The region @id.')
parser.add_argument('--column', type=str, required=True,
                    help='The name of the column to update. Example: "term.name".')
parser.add_argument('--value', type=str, required=True,
                    help='The value to set in the column.')
args = parser.parse_args()


def main():
    region = pd.read_excel(join(SRC_DIR, filename))
    col_name = f"term.{args.column.replace('term.', '')}"  # make sure column starts with `term.`
    region.loc[region['term.id'] == args.term_id, [col_name]] = args.value
    region.to_excel(join(SRC_DIR, filename), index=None, header=True)


if __name__ == "__main__":
    main()
