import os
import sys
import re

LOOKUP_DIR = 'lookups'
IGNORE_VALUES = ['term.id', 'term.name']
regex = re.compile(r'^[A-Za-z\d_-]*$')


def _valid_file(filename: str):
    return not filename.startswith('region-') and not '-siteType-' in filename


def _get_files(folder: str):
    return list(map(
        lambda filename: os.path.join(folder, filename),
        list(filter(lambda file: file.endswith('.csv') and _valid_file(file), os.listdir(folder)))
    ))


def _invalid_header(header: str):
    return header not in IGNORE_VALUES and not regex.match(header)


def _validate_file(filepath: str):
    with open(filepath) as f:
        headers = f.readline().replace('\n', '').split(',')

    return list(filter(_invalid_header, headers))


def main():
    files = _get_files(LOOKUP_DIR)

    exit_code = ''

    for filepath in files:
        errors = _validate_file(filepath)
        if len(errors) > 0:
            err = '\n\t'.join(errors)
            exit_code += f"\nInvalid names for lookups in '{filepath}':\n\t{err}"

    sys.exit(0 if exit_code == '' else exit_code)


if __name__ == "__main__":
    main()
