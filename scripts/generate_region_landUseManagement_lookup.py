# install: `pip -r install scripts/requirements-gadm.txt`
# add file `ee-credentials.json` to authenticate to Google Earth Engine
# usage: `python scripts/generate_region_landUseManagement_lookup.py --gadm-level=0 --limit=100`
import argparse
import os
from functools import reduce
import pandas as pd
from hestia_earth.earth_engine import init_gee, run


parser = argparse.ArgumentParser(description='Generate "region-landUseManagement" lookup file')
parser.add_argument('--gadm-level', type=int,
                    help='Restrict regions by level')
parser.add_argument('--limit', type=int,
                    help='Maximum number of regions to process at once.')
args = parser.parse_args()


GADM_LEVELS = [
    0, 1
]
TERM_IDS = [
    'longFallowRatio',
    'croppingIntensity'
]
OUTPUT_DIR = 'Geographies'
FILENAME = 'region-landUseManagement-lookup.csv'
FILEPATH = os.path.join(OUTPUT_DIR, FILENAME)


def _non_empty_list(values: list): return list(filter(lambda x: len(x) > 0, values))


def _all_gadm_ids(level: str):
    with open(f"./scripts/GADM-ID-level{level}.txt", 'r') as f:
        return _non_empty_list(f.read().split('\n'))


def _processed_gadm_ids():
    df = pd.read_csv(FILEPATH, na_values='-', index_col=None)
    return set(df['term.id'].values.tolist())


def _exec(term_id: str, data: dict):
    res = run({
        **data,
        'gadm_id': term_id,
        'max_area': 10000000
    })
    return res.get('features', [{'properties': {}}])[0].get('properties')


def _get_croppingIntensity(term_id: str):
    EE_PARAMS = {
        'ee_type': 'raster',
        'reducer': 'sum',
        'fields': 'sum'
    }

    value = _exec(term_id, {
        **EE_PARAMS,
        'collection': 'users/hestiaplatform/MMGA'
    })
    MMGA_value = value.get(EE_PARAMS['reducer'], 0)

    value = _exec(term_id, {
        **EE_PARAMS,
        'collection': 'users/hestiaplatform/AH'
    })
    AH_value = value.get(EE_PARAMS['reducer'])

    return None if any([MMGA_value is None, AH_value is None, AH_value == 0]) else (MMGA_value / AH_value)


def _get_longFallowRatio(term_id: str):
    EE_PARAMS = {
        'ee_type': 'raster',
        'reducer': 'mean',
        'fields': 'mean'
    }

    value = _exec(term_id, {
        **EE_PARAMS,
        'collection': 'users/hestiaplatform/MMGA'
    })
    MMGA_value = value.get(EE_PARAMS['reducer'], 0)

    value = _exec(term_id, {
        **EE_PARAMS,
        'collection': 'users/hestiaplatform/CE'
    })
    CE_value = value.get(EE_PARAMS['reducer'])

    return None if any([MMGA_value is None, CE_value is None, MMGA_value == 0]) else min(6, max(CE_value / MMGA_value, 1))


GET_BY_TERM = {
    'croppingIntensity': _get_croppingIntensity,
    'longFallowRatio': _get_longFallowRatio
}


def _process_term_id(term_id: str):
    return reduce(lambda p, c: p + [GET_BY_TERM.get(c, lambda *args: None)(term_id)], TERM_IDS, [])


def main():
    init_gee()

    levels = [args.gadm_level] if args.gadm_level is not None else GADM_LEVELS

    term_ids = reduce(lambda p, c: p + _all_gadm_ids(c), levels, [])
    existing_term_ids = _processed_gadm_ids()
    new_term_ids = [term_id for term_id in term_ids if term_id not in existing_term_ids]

    new_term_ids = new_term_ids[0:args.limit] if args.limit else new_term_ids

    print('Processing', len(new_term_ids), 'new regions for levels', levels)

    values = reduce(lambda p, c: {**p, c: _process_term_id(c)}, new_term_ids, {})

    existing_df = pd.read_csv(FILEPATH, na_values='-', index_col='term.id')
    new_df = pd.DataFrame.from_dict(values, orient='index', columns=TERM_IDS)
    new_df.index.rename('term.id', inplace=True)
    df = pd.concat([existing_df, new_df])
    df.to_csv(FILEPATH, na_rep='-')


if __name__ == "__main__":
    main()
