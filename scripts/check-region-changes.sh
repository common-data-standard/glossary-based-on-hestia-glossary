#!/usr/bin/env bash

set -e

COMMIT_SHA="$1"
SRC_FILE="Geographies/region.xlsx"
SRC_FOLDER="gadm"

DIFFS=$(git diff --name-only $COMMIT_SHA origin/develop) || ""
if [[ "$DIFFS" == *"$SRC_FILE"* ]]; then
  if [[ "$DIFFS" != *"$SRC_FOLDER"* ]]; then
    echo "Changes have been detected in ${SRC_FILE}, but no changes in ${SRC_FOLDER} folder." 1>&2
    echo "The ${SRC_FILE} is auto-generated from scripts in ./${SRC_FOLDER}, hence changes should be present in at least one of those files." 1>&2
    exit 1
  fi
fi
