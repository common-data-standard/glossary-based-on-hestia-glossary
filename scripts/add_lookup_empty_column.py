import sys
import pandas as pd


def main(args):
    file = args[0]
    col_name = args[1]
    df = pd.read_csv(file, index_col=None)

    df[col_name] = '-'

    df.to_csv(file, index=False)


if __name__ == "__main__":
    main(sys.argv[1:])
