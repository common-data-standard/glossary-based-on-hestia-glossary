# Rename `term.id` column for file downloaded from FAOSTAT

> Replaces the names of the regions by the term `@id` in the column `term.id`

## Usage

1. Follow instructions on [reformat script](FAOSTAT_reformat_region_product_data.md) to format the file correctly
2. Run `python3 scripts/FAOSTAT_rename_termid.py data/output.csv`

When the process is done, the file `data/output.csv` will be updated.
