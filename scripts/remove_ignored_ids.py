# script to remove rows on the region.xlsx file that match the ids in gadm/ignore.txt
# install python packages: pandas openpyxl
from os.path import dirname, join, abspath
import sys
import pandas as pd


CURRENT_DIR = dirname(abspath(__file__))
SRC_DIR = join(CURRENT_DIR, '..', 'Geographies')


def non_empty_list(values: list): return list(filter(lambda x: len(x) > 0, values))


def main(args):
    ignore_file = args[0] if len(args) > 0 else join(CURRENT_DIR, '..', 'gadm', 'ignore.txt')

    with open(ignore_file, 'r') as f:
        ignore_ids = non_empty_list(f.read().split('\n'))

    filename = 'region'
    df = pd.read_excel(join(SRC_DIR, f"{filename}.xlsx"))

    df[~df['term.id'].isin(ignore_ids)].to_excel(join(SRC_DIR, f"{filename}-ignored.xlsx"), index=None, header=True)


if __name__ == "__main__":
    main(sys.argv[1:])
